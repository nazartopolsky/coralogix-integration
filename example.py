import logging
import os
from src import configure, get_logger

configure(
    batch_size=1000,
    coralogix_private_key=os.environ['CORALOGIX_PRIVATE_KEY'],
)

logger = logging.getLogger(__name__)
logger.addHandler(get_logger('app', 'subsystem'))
logger.setLevel(logging.INFO)

if __name__ == '__main__':
    logger.info('A message.')
    logger.warning('Something might be bad...')
    logger.error('AN ERROR!')
