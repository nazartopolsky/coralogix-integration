from enum import Enum


class Severity(Enum):
    DEBUG = 1
    VERBOSE = 2
    INFO = 3
    WARN = 4
    ERROR = 5
    CRITICAL = 6


class LogEntry(object):
    timestamp = None
    severity = 1
    text = ''
    category = None
    class_name = None
    method_name = None
    thread_id = None

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            if not hasattr(self, k):
                raise AttributeError
            setattr(self, k, v)
        self._validate()

    def _validate(self):
        if self.timestamp is None:
            raise ValueError('Timestamp required')
        self.severity = Severity(self.severity)
        if self.text is None:
            raise ValueError('Text required')
