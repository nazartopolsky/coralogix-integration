import atexit
from collections import defaultdict
from .conf import settings


class LogCollector(object):
    def __init__(self, sender):
        self.logs = defaultdict(list)
        self.sender = sender
        self._register_at_exit()

    def add_entry(self, app_name, subsystem, entry):
        log_list = self.logs[(app_name, subsystem)]
        if len(log_list) < self._batch_size:
            log_list.append(entry)
        else:
            self.logs[(app_name, subsystem)] = [entry]
            self.send_logs(app_name, subsystem, log_list)

    def send_logs(self, app_name, subsystem, logs):
        self.sender.send(app_name, subsystem, logs)

    def send_all(self):
        logs = self.logs
        self.logs = defaultdict(list)
        for app_name, subsystem in logs:
            entries = logs[app_name, subsystem]
            if entries:
                self.send_logs(app_name, subsystem, entries)

    @property
    def _batch_size(self):
        return settings.batch_size

    def _register_at_exit(self):
        atexit.register(self.send_all)
