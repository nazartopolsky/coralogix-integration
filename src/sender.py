import json
from urllib.request import Request, urlopen
from .conf import settings
from .encoder import LogEntryEncoder


class CoralogixSender(object):
    json_encoder = LogEntryEncoder

    content_type = 'application/json'
    endpoint = 'https://api.coralogix.com/api/v1/logs'

    def __init__(self, encoder):
        self.json_encoder = encoder

    def send(self, app_name, subsystem, logs):
        request = self._make_request(app_name, subsystem, logs)
        f = urlopen(request)
        f.read()
        f.close()

    def _make_request(self, app_name, subsystem, logs):
        return Request(
            self.endpoint,
            data=self._get_body(app_name, subsystem, logs),
            headers={'Content-Type': self.content_type},
        )

    def _get_body(self, app_name, subsystem, logs):
        body = {
            'privateKey': self._private_key,
            'applicationName': app_name,
            'subsystemName': subsystem,
            'logEntries': logs,
        }
        return json.dumps(body, cls=self.json_encoder).encode('utf-8')

    @property
    def _private_key(self):
        return settings.coralogix_private_key
