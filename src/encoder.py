import json
from .models import Severity, LogEntry


class LogEntryEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, LogEntry):
            return {to_camel_case(k): v for k, v in o.__dict__.items()}
        if isinstance(o, Severity):
            return o.value
        return super().default(o)


def to_camel_case(s):
    components = s.split('_')
    return components[0] + ''.join(x.title() for x in components[1:])
