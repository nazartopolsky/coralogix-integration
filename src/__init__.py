from .collector import LogCollector
from .conf import configure
from .handler import CoralogixHandler
from .encoder import LogEntryEncoder
from .sender import CoralogixSender


def get_logger(app_name, subsystem):
    return CoralogixHandler(app_name, subsystem, _log_list)


_log_list = LogCollector(CoralogixSender(LogEntryEncoder))
