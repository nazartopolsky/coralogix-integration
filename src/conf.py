class Settings(object):
    batch_size = 1000
    coralogix_private_key = ''


settings = Settings()


def configure(**kwargs):
    for k, v in kwargs.items():
        setattr(settings, k, v)
