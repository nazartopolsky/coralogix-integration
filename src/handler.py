import logging
from .models import LogEntry


class CoralogixHandler(logging.Handler):
    def __init__(self, app_name, subsystem, log_list):
        super().__init__()
        self.app_name = app_name
        self.subsystem = subsystem
        self.log_list = log_list

    def emit(self, record):
        log_entry = self._convert_record(record)
        self.log_list.add_entry(self.app_name, self.subsystem, log_entry)

    def _convert_record(self, record: logging.LogRecord):
        return LogEntry(
            timestamp=record.created * 1000,
            severity=int(record.levelno / 10),
            text=record.msg,
            thread_id=record.thread,
            class_name=record.module,
            method_name=record.funcName,
        )
