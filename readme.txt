An example of logging handler that sends logs to Coralogix REST API.

Uses message queue (with configurable batch size).

Written with Python 3.5 in mind, no external dependencies.
